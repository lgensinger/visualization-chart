import path from "path";
import { dirname } from "path";
import { fileURLToPath } from "url";

import webpack from "webpack";

const __dirname = dirname(fileURLToPath(import.meta.url));

const commonConfig = {

    entry: {
        index: "./src/index.js"
    },

    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                "LGV_HEIGHT": JSON.stringify(process.env.LGV_HEIGHT),
                "LGV_PACK_PADDING": JSON.stringify(process.env.LGV_PACK_PADDING),
                "LGV_PARSE_DELIMITER": JSON.stringify(process.env.LGV_PARSE_DELIMITER),
                "LGV_RING_INNER_RADIUS": JSON.stringify(process.env.LGV_RING_INNER_RADIUS),
                "LGV_SERIES_LABELS": JSON.stringify(process.env.LGV_SERIES_LABELS),
                "LGV_SERIES_LABELS_ON_TOP": JSON.stringify(process.env.LGV_SERIES_LABELS_ON_TOP),
                "LGV_SERIES_NORMAILIZE": JSON.stringify(process.env.LGV_SERIES_NORMAILIZE),
                "LGV_SERIES_PADDING": JSON.stringify(process.env.LGV_SERIES_PADDING),
                "LGV_WIDTH": JSON.stringify(process.env.LGV_WIDTH)
            }
        })
    ],

    output: {
        filename: "visualization-chart.bundle.js",
        path: path.resolve(__dirname, "dist"),
        clean: true
    }

 };

 export { commonConfig };
 export default commonConfig;
