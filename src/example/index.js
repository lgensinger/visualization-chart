import hljs from "highlight.js/lib/core";
import javascript from "highlight.js/lib/languages/javascript";

// register syntax highligher
hljs.registerLanguage("javascript", javascript);

// output language
const comment = "perform the described action to see an example\n// of the data available to that DOM event";

/**
 * Process visualization events.
 * @param {event} event - Javascript Event object
 */
function processEvent(event) {
    event.currentTarget.outputContainer.innerHTML = hljs.highlight(
        `// ${event.type}\n${JSON.stringify(event.detail,null,2)}`,
        { language: "javascript" }
      ).value
}

/**
 * Render souce data and event output
 * @param {array} data - object with key/value pairs of path
 * @param {element} outputContainer - HTML element to populate with event output data code block
 * @param {element} sourceContainer - HTML element to populate with source data code block
 */
function renderDefault(data,sourceContainer,outputContainer) {

    // data
    sourceContainer.innerHTML = hljs.highlight(
        JSON.stringify(data.filter((d,i) => i < 3),null,2),
        { language: "javascript" }
      ).value;

    // output
    outputContainer.innerHTML = hljs.highlight(
        `// ${comment}`,
        { language: "javascript" }
      ).value;
    
}

export { comment, processEvent, renderDefault };