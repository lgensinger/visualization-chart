import { sum } from "d3-array";
import { pack } from "d3-hierarchy";
import { scaleSqrt } from "d3-scale";

import { configurationLayout } from "../configuration.js";
import { TreeData } from "../structure/tree.js";

/**
 * BubbleLayout is a data-bound layout abstraction for a bubble chart.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {object} params - key/value pairs used for conditioning
 */
class BubbleLayout extends TreeData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.height = params.height ? params.height : configurationLayout.height;
        this.padding = params.padding ? params.padding : 1;
        this.width = params.width ? params.width : configurationLayout.width;
        this.data = this.update;

    }

    /**
     * Construct a radius scale.
     * @returns A d3 scale function.
     */
    get rScale() {
        return scaleSqrt()
            .domain([0, sum(this.source, d => d.value) * 2])
            .range([0, this.width / 2]);
    }

    /**
     * Construct layout.
     * @returns A d3 pack layout function.
     */
    get layout() {
        return pack()
            .radius(d => this.rScale(this.extractValue(d)))
            .size([this.width, this.height])
            .padding(this.padding);
    }

    /**
     * Build data object intersecting with layout parameters.
     * @returns A d3 pack data object.
     */
    get update() {
        return this.layout(this.condition(this.source,this.params));
    }

    /**
     * Get radius from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum radius value.
     */
    extractRadius(d) {
        return d.r;
    }

}

export { BubbleLayout };
export default BubbleLayout;
