import { group, rollup, sum } from "d3-array";
import moment from "moment";

import { TimeData } from "../structure/time.js";

moment.updateLocale('en', {
    week: {
      dow : 0, // Monday is the first day of the week.
      doy: 6
    }
  });

/**
 * CalendarWeekLayout is a data-bound layout abstraction for a calendar chart.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {object} params - key/value pairs needed for getters
 */
class CalendarWeekLayout extends TimeData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.data = this.update;

    }

    /**
     * Condition data for visualization requirements.
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    condition(data, params) {

        // add ids if not provided
        let datum = this.enrichId(data);

        // aggregate activity types
        let aggregate = rollup(datum,
            v => v.length,
            d => moment(d.date).format("YYYY-MM-DD"),
            d => d.label
        );

        return params.activityTypes.map(d => {
            return Array.from(aggregate)
                .map(x => [x[0], Object.fromEntries(x[1])])
                .filter(x => Object.keys(x[1]).includes(d))
                .map(x => [x[0], x[1][d]])
                .map(x => x.concat([d]));
        }).flat();

    }

}

/**
 * CalendarYearLayout is a data-bound layout abstraction for a calendar chart by year with month/day of week grid.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {object} params - key/value pairs needed for getters
 */
class CalendarYearLayout extends TimeData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.unit = typeof window === "undefined" ? 16 : parseFloat(getComputedStyle(document.body).fontSize);
        this.data = this.update;
        this.yearPadding = this.unit;
        this.yearHeight = (this.params.height / this.data.size) - (this.yearPadding * ((this.data.size - 1) / this.data.size));
        this.cellHeight = this.yearHeight / this.weekdays.length;
        this.cellWidth = (this.params.width - this.unit) / 54;
        this.aggregate = rollup(this.source, v => sum(v, d => 1), d => d.date);
        this.lookup = group(this.source, d => d.date);

    }

    /**
     * Condition data for visualization requirements.
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    condition(data, params) {
        
        // add ids if not provided
        let datum = data ? data.map((d,i) => {

            let result = d;

            if (!Object.keys(d).includes("id")) {
                result.id = i;
            }

            return d;

        }) : data;

        return group(data, d => moment(d.date).year());

    }

    /**
     * Get id from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum id.
     */
    extractId(d) {
        return d;
    }

    /**
     * Get value from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum value.
     */
    extractValue(d) {
        let item = this.aggregate.get(d);
        return typeof(d) == "string" ? (item ? item : 0) : d.count;
    }

}

export { CalendarWeekLayout, CalendarYearLayout };
export default CalendarYearLayout;
