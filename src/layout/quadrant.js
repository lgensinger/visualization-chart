import { max, min } from "d3-array";
import { ScatterData } from "../structure/scatter.js";

/**
 * QuadrantLayout is a data-bound layout abstraction for an x/y quadrant chart.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {array} x - min, max, label, tick count for x axis
 * @param {array} y - min, max, label, tick count for y axis
 */
class QuadrantLayout extends ScatterData {

    constructor(data, x=[null, null, "x", null], y=[null, null, "y", null]) {

        // initialize inheritance
        super(data);

        // update self
        this.data = this.update;
        this.x = x;
        this.y = y;

    }

    /**
     * Determine x label.
     * @returns A string.
     */
    get xLabel() {
        return this.x[2];
    }

    /**
     * Determine x maxium.
     * @returns A float.
     */
    get xMax() {
        return this.x[1] != null ? this.x[1] : max(this.data, d => d.x);
    }

    /**
     * Construct x minimum.
     * @returns A float.
     */
    get xMin() {
        return this.x[0] != null ? this.x[0] : min(this.data, d => d.x);
    }

    /**
     * Construct tick thresholds.
     * @returns An integer.
     */
    get xTicks() {
        return this.x[3] != null ? this.x[3] : this.xMax;
    }

    /**
     * Determine y label.
     * @returns A string.
     */
    get yLabel() {
        return this.y[2];
    }

    /**
     * Construct y max.
     * @returns A float.
     */
    get yMax() {
        return this.y[1] != null ? this.y[1] : max(this.data, d => d.y);
    }

    /**
     * Construct y minimum.
     * @returns A float.
     */
    get yMin() {
        return this.y[0] != null ? this.y[0] : min(this.data, d => d.y);
    }

    /**
     * Construct tick thresholds.
     * @returns An integer.
     */
    get yTicks() {
        return this.y[3] != null ? this.y[3] : this.yMax;
    }

    /**
     * Generate event detail object.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @param {q} string - quadrant label
     * @returns An object of key/value metadata pairs.
     */
    configureEventDetails(d,e,q) {
        return {
            id: this.extractId(d),
            label: this.extractLabel(d),
            quadrant: q,
            x: d.x,
            y: d.y,
            xy: [e.clientX, e.clientY]
        };
    }

    /**
     * Determine quadrant label.
     * @param {d} object - d3.js data object
     * @param {xDomain} float - value of x domain
     * @param {yDomain} float - value of y domain
     * @returns A string representing the human-legible quadrant label.
     */
    extractQuadrant(d, xDomain, yDomain) {

        let x = d.x < (xDomain / 2) ? "low" : "high";
        let y = d.y < (yDomain / 2) ? "low" : "high";

        return `${x} ${this.xLabel} / ${y} ${this.yLabel}`;

    }

}

export { QuadrantLayout };
export default QuadrantLayout;
