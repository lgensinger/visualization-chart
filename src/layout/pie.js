import { arc, pie } from "d3-shape";

import { configurationLayout } from "../configuration.js";
import { ChartData } from "../structure/index.js";

/**
 * PieLayout is a data-bound layout abstraction for a pie chart.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {object} params - key/value pairs needed for getters
 */
class PieLayout extends ChartData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.height = params.height ? params.height : configurationLayout.height;
        this.width = params.width ? params.width : configurationLayout.width;
        this.data = this.update;

    }

    /**
     * Construct layout.
     * @returns A d3 pack layout function.
     */
    get layout() {
        return pie()
            .value(d => this.extractValue(d));
    }

    /**
     * Build data object intersecting with layout parameters.
     * @returns A d3 pack data object.
     */
    get update() {
        return this.layout(this.condition(this.source));
    }

    /**
     * Get label from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum label.
     */
    extractLabel(d) {
        return d.data.label;
    }

}

export { PieLayout };
export default PieLayout;
