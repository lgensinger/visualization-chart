import { forceCollide, forceManyBody, forceSimulation, forceX, forceY } from "d3-force";

import { configurationLayout } from "../configuration.js";
import { BubbleLayout } from "../layout/bubble.js";
import { ChartData } from "../structure/index.js";

/**
 * BubbleForceLayout is a data-bound layout abstraction for a force layout chart.
 * @param {array} data - object with key/value pairs of id, label, value
 */
class BubbleForceLayout extends ChartData {

    constructor(data, width, height, unit) {

        // initialize inheritance
        super(data);

        // update self
        this.Bubble = new BubbleLayout(data, width, height);
        this.height = height;
        this.unit = unit;
        this.width = width;

        this.data = this.Bubble.data.descendants();

        // const force layout
        this.FORCE = forceSimulation()
            .force("charge", forceManyBody().strength(d => Math.pow(this.extractRadius(d), 2.0) * 0.01))
            .force("collide", forceCollide().radius(d => this.extractRadius(d) + 1))
            .force("x", forceX().strength(0.03).x(d => this.test ? 50 : this.width / 2))
            .force("y", forceY().strength(0.03).y(d => this.test ? 100 : this.height / 2))
            .stop();

    }

}

export { BubbleForceLayout };
export default BubbleForceLayout;
