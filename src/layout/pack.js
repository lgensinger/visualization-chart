import { max } from "d3-array";
import { pack } from "d3-hierarchy";

import { configurationData, configurationLayout } from "../configuration.js";
import { TreeData } from "../structure/tree.js";

/**
 * PackLayout is a data-bound layout abstraction for a packed circles chart.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {string} delimiter - parse delimiter
 * @param {integer} height - artboard height
 * @param {float} padding - spacing within layout between elements
 * @param {integer} width - artboard width
 */
class PackLayout extends TreeData {

    constructor(data, width=configurationLayout.width, height=configurationLayout.height, padding=0.4, delimiter=configurationData.delimiter) {

        // initialize inheritance
        super(data, {delimiter: delimiter});

        // update self
        this.height = height;
        this.maxDepth = max([...new Set(this.data.descendants().map(d => d.depth))]);
        this.padding = padding;
        this.width = width;
        this.data = this.update

    }

    /**
     * Construct layout.
     * @returns A d3 pack layout function.
     */
    get layout() {
        return pack()
            .size([this.width, this.height])
            .padding(this.padding);
    }

    /**
     * Build data object intersecting with layout parameters.
     * @returns A d3 pack data object.
     */
    get update() {
        return this.layout(this.condition(this.source, this.params));
    }

}

export { PackLayout };
export default PackLayout;
