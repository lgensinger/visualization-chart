import { forceCollide, forceSimulation, forceX, forceY } from "d3-force";

import { configurationLayout } from "../configuration.js";
import { ClusterData } from "../structure/cluster.js";

/**
 * ForceLayout is a data-bound layout abstraction for a force layout chart.
 * @param {array} data - object with key/value pairs of id, label, value
 */
class ForceLayout extends ClusterData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.data = this.update;
        this.rScale = params ? params.rScale : null;
        this.xScale = params ? params.xScale : null;
        this.yScale = params ? params.yScale : null;

    }

    /**
     * Construct layout.
     * @returns A d3 force simulation function.
     */
    get layout() {
        return forceSimulation(this.data)
            .force("collide", forceCollide().radius(d => this.rScale(this.extractRadius(d) + 1)))
            .force("x", forceX().x(d => this.xScale(this.extractX(d))))
            .force("y", forceY().y(d => this.yScale(this.extractY(d))))
            .stop();
    }

}

export { ForceLayout };
export default ForceLayout;
