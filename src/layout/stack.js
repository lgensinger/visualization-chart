import { max, sum } from "d3-array";
import { stackOffsetExpand, stackOffsetNone, stack } from "d3-shape";

import { configurationData, configurationLayout, configurationSeries } from "../configuration.js";
import { SeriesData } from "../structure/series.js";

/**
 * StackLayout is a data-bound layout abstraction for a stacked series chart.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {object} params - key/value pairs needed for getters
 */
class StackLayout extends SeriesData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.height = params.height ? params.height : configurationLayout.height;
        this.labelsOnTop = params.labelsOnTop != undefined ? params.labelsOnTop : configurationSeries.labelsOnTop;
        this.normalize = params.normalize != undefined ? params.normalize : configurationSeries.normalize;
        this.padding = params.paddingSeries !== undefined && params.paddingSeries !== null ? params.paddingSeries : configurationSeries.padding;
        this.useLabels = params.useLabels != undefined ? params.useLabels : configurationSeries.useLabels;
        this.width = params.width ? params.width : configurationLayout.width;
        this.data = this.update;

    }

    /**
     * Construct layout.
     * @returns A d3 stack layout function.
     */
    get layout() {
        return stack()
            .keys(d => Object.keys(d[0]))
            .offset(this.normalize ? stackOffsetExpand : stackOffsetNone)
            .value((d,k) => d[k].value);
    }

    /**
     * Build data object intersecting with layout parameters.
     * @returns A d3 stack data object.
     */
    get update() {

        // pull series labels
        this.seriesLabels = Object.keys(this.source);

        // determine max across all series
        this.seriesMax = max(
            this.seriesLabels.map(k =>
                Object.keys(this.source[k]).map(kk => this.source[k][kk])
            ).map(d => sum(d))
        );

        return this.seriesLabels.map(k => {

            let o = {};

            // map richer data object into format for stack
            Object.keys(this.source[k]).forEach(kk => o[kk] = this.condition(this.source)[k][kk]);

            return {
                series: k,
                values: this.layout([o])
            }

        });

    }

    /**
     * Get id from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum id.
     */
    extractId(d) {
        return d[0].data[d.key].id;
    }

    /**
     * Get label from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum label.
     */
    extractLabel(d) {
        return d.key;
    }

    /**
     * Get series name from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a series name.
     */
    extractSeries(d) {
        return d[0].data[d.key].series;
    }

    /**
     * Get value from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum value.
     */
    extractValue(d) {
        return d[0].data[d.key].value;
    }

}

export { StackLayout };
export default StackLayout;
