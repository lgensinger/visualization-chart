import { max } from "d3-array";
import { tree } from "d3-hierarchy";

import { configurationData, configurationLayout } from "../configuration.js";
import { TreeData } from "../structure/tree.js";

/**
 * TreeLayout is a data-bound layout abstraction for a packed circles chart.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {string} delimiter - parse delimiter
 * @param {integer} height - artboard height
 * @param {float} padding - spacing within layout between elements
 * @param {integer} width - artboard width
 */
class TreeLayout extends TreeData {

    constructor(data, width=configurationLayout.width, height=configurationLayout.height, padding=100, dotSize=null, delimiter=configurationData.delimiter) {

        // initialize inheritance
        super(data, { delimiter: delimiter });

        // update self
        this.delimiter = delimiter;
        this.dotSize = dotSize;
        this.height = height;
        this.maxDepth = max([...new Set(this.data.descendants().map(d => d.depth))]);
        this.padding = padding;
        this.width = width;
        this.data = this.update

    }

    /**
     * Construct layout.
     * @returns A d3 tree layout function.
     */
    get layout() {
        return tree()
            .size([2 * Math.PI, this.width - (this.dotSize * 2)])
            .separation((a, b) => (a.parent == b.parent ? 1 : this.padding) / a.depth);
    }

    /**
     * Build data object intersecting with layout parameters.
     * @returns A d3 pack data object.
     */
    get update() {
        return this.layout(this.condition(this.source));
    }

}

export { TreeLayout };
export default TreeLayout;
