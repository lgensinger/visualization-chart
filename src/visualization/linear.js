import { VisualizationChart } from "./index.js";
import { configuration, configurationLayout } from "../configuration.js";

/**
 * LinearGrid is a chart abstraction that uses a linear coordinate system.
 * @param {class} ChartData - lgv data class
 * @param {object} data - usually array; occasionally object
 * @param {string} label - identifer for chart brand
 * @param {float} height - specified height of chart
 * @param {string} name - name of chart
 * @param {float} width - specified width of chart
 */
class LinearGrid extends VisualizationChart {

    constructor(data, width=configurationLayout.width, height=configurationLayout.height, ChartData=null, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, width, height, ChartData, label, name);

    }

}

export { LinearGrid };
export default LinearGrid;
