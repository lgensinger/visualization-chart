import { interpolateRound, interpolateString } from "d3-interpolate";
import { select, selectAll } from "d3-selection";

import { configuration, configurationLayout } from "../configuration.js";
import { ChartLabel } from "../label/index.js";
import { ChartData as CD } from "../structure/index.js";

/**
 * VisualizationChart is a chart abstraction.
 * @param {class} ChartData - lgv data class
 * @param {object} data - usually array; occasionally object
 * @param {string} label - identifer for chart brand
 * @param {float} height - specified height of chart
 * @param {string} name - name of chart
 * @param {float} padding - spacing within layout between elements
 * @param {float} width - specified width of chart
 */
class VisualizationChart {

    constructor(data, width=configurationLayout.width, height=configurationLayout.height, ChartData=null, label=configuration.branding, name=configuration.name) {

        // update self
        this.artboard = null;
        this.branding = label;
        this.classContainer = `${label}-content`;
        this.Data = ChartData ? ChartData : new CD(data);
        this.data = this.Data.data;
        this.container = null;
        this.content = null;
        this.heightSpecified = height;
        this.Label = null;
        this.name = name;
        this.prefix = label;
        this.widthSpecified = width;

    }

    /**
     * Determine artboard height.
     * @returns A float representing the height of the svg element.
     */
    get height() {
        return this.heightSpecified;
    }

    /**
     * Determine artboard unit size based on font size as the base unit of measure make responsiveness easier to manage across devices.
     * @returns A float representing the unit size a grided artboard.
     */
    get unit() {
        return typeof window === "undefined" ? 16 : parseFloat(getComputedStyle(document.body).fontSize);
    }

    /**
     * Determine artboard width.
     * @returns A float representing the width of the svg element.
     */
    get width() {
        return this.widthSpecified;
    }

    /**
     * Position and minimally style SVG dom element.
     */
    configureArtboard() {
        this.artboard
        .attr("class", `${this.prefix}-${this.name}`)
            .attr("viewBox", d => `0 0 ${d.width} ${d.height}`);
    }

    /**
     * Position and minimally style containing group in SVG dom element.
     */
    configureContainer() {
        this.content
            .attr("class", this.classContainer);
    }

    /**
     * Configure a JavaScript event.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @param {id} string - event id
     */
    configureEvent(id, d, e, z) {
        this.artboard.dispatch(id, {
            bubbles: true,
            detail: this.Data.configureEventDetails(d,e,z)
        });
    }

    /**
     * Method to allow bulk overwrites of arbitrary items.
     */
    configureOverwrites() {
    }

    /**
     * Generate SVG artboard in the HTML DOM.
     * @param {selection} domNode - d3 selection
     * @returns A d3.js selection.
     */
    generateArtboard(domNode) {
        return domNode
            .selectAll("svg")
            .data([{height: this.height, width: this.width}])
            .join(
                enter => enter.append("svg"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate SVG group to hold content that can not be trimmed by artboard.
     * @param {selection} domNode - d3 selection
     * @returns A d3.js selection.
     */
    generateContainer(domNode) {
        return domNode
            .selectAll(`.${this.classContainer}`)
            .data(d => [d])
            .join(
                enter => enter.append("g"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate main visualization, i.e. everything that sits inside the svg.
     */
    generateChart() {

        // initialize labeling
        this.Label = new ChartLabel(this.artboard, this.Data);

    }

    /**
     * Generate artboard and container for visualization.
     */
    generateCore() {

        // generate svg artboard
        this.artboard = this.generateArtboard(this.container);
        this.configureArtboard();

        // wrap for content to ensure render within artboard
        this.content = this.generateContainer(this.artboard);
        this.configureContainer();

    }

    /**
     * Generate patterns for visualization.
     */
    generatePatterns() {

    }

    /**
     * Generate visualization.
     * @param {styles} object - key/value pairs where each key is a CSS property and corresponding value is its value
     */
    generateVisualization(styles) {

        // generate core elements
        this.generateCore();

        // generate patterns
        this.generatePatterns();

        // generate chart
        this.generateChart();

        // allow overwrites to facilitate outside of complete method overwriting
        this.configureOverwrites();

    }

    /**
     * Render visualization in dom.
     * @param {node} domNode - HTML node
     * @param {styles} object - key/value pairs where each key is a CSS property and corresponding value is its value
    */
    render(domNode, styles) {

        // update self
        this.container = select(domNode);

        // generate visualization
        this.generateVisualization();

    }

    /**
     * Generate intermediate values for a label between a start and end state.
     * @param {bounds} float - amount of space for the label to render
     * @param {index} integer - index of label (meaningful if a label partial)
     * @param {label} string - text label
     * @param {value} float - numeric value
     * @returns A d3.js tween function.
     */
    tweenText(label, value, bounds, index=0) {

        // set initial
        this._current = 0;

        // determine label
        let l = this.Label.format(index, label, value, bounds);

        // how to interpolate
        let isNumber = typeof(l) == "number";

        // set up interpolator
        const iText = index == 0 ? interpolateString(this._current, l) : (isNumber ? interpolateRound(this._current, l) : interpolateString(this._current, l));

        return t => this._current = iText(t);

    }

    /**
     * Update visualization.
     * @param {object} data - key/values where each key is a
    series label and corresponding value is an array of values
     * @param {float} height - specified height of chart
     * @param {float} width - specified width of chart
     */
    update(data, width, height) {

        // update self
        this.heightSpecified = height || this.heightSpecified;
        this.widthSpecified = width || this.widthSpecified;

        // if layout data object
        if (this.Data.height && this.Data.width) {

            // update layout attributes
            this.Data.height = this.heightSpecified;
            this.Data.width = this.widthSpecified;

        }

        // recalculate values
        this.Data.source = data;
        this.Data.data = this.Data.update;
        this.data = this.Data.data;

        // generate visualization
        this.generateVisualization();

    }

}

export { VisualizationChart };
export default VisualizationChart;
