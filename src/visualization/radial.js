import { VisualizationChart } from "./index.js";
import { configuration, configurationLayout } from "../configuration.js";

/**
 * RadialGrid is a chart abstraction that uses a radial coordinate system.
 * @param {class} ChartData - lgv data class
 * @param {object} data - usually array; occasionally object
 * @param {string} label - identifer for chart brand
 * @param {float} height - specified height of chart
 * @param {string} name - name of chart
 * @param {float} width - specified width of chart
 */
class RadialGrid extends VisualizationChart {

    constructor(data, width=configurationLayout.width, height=configurationLayout.height, ChartData=null, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, width, height, ChartData, label, name);

    }

    /**
     * Determine radial orthant label falls into for positioning.
     * @returns An integer (1-8) representing the half of quadrant or 1/8th of circle a degree falls into.
     */
    get orthant() {
        return d => {

            let result = null;
        
            if (d >= 0 && d <= 45) result = 1;
            if (d > 45 && d <= 90) result = 2;
            if (d > 90 && d <= 135) result = 3;
            if (d > 135 && d <= 180) result = 4;
            if (d > 180 && d <= 225) result = 5;
            if (d > 225 && d <= 270) result = 6;
            if (d > 270 && d <= 315) result = 7;
            if (d > 315) result = 8;

            return result;

        };
    }

    /**
     * Calculate radius.
     * @returns A float representing the radius of the outer most circle.
     */
    get radius() {
        return Math.min( ...[this.height, this.width]) / 2;
    }

    /**
     * Position and minimally style containing group in SVG dom element.
     */
    configureContainer() {
        this.content
            .attr("class", this.classContainer)
            .attr("transform", `translate(${this.width / 2},${this.height / 2})`);
    }

}

export { RadialGrid };
export default RadialGrid;
