import { ChartData } from "../structure/index.js";
import { VisualizationChart } from "./index.js";
import { configuration } from "../configuration.js";

/**
 * DomGrid is a chart abstraction that uses a HTML document object model (DOM) coordinate system.
 * @param {class} ChartData - lgv data class
 * @param {object} data - usually array; occasionally object
 * @param {string} label - identifer for chart brand
 * @param {float} height - specified height of chart
 * @param {string} name - name of chart
 * @param {float} width - specified width of chart
 */
class DomGrid extends VisualizationChart {

    constructor(data, ChartData=null, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, null, null, ChartData, label, name);

    }

    /**
     * Position and minimally style DOM element.
     */
    configureArtboard() {
        this.artboard
            .attr("class", `${this.prefix}-${this.name}`);
    }

    /**
     * Position and minimally style containing group in SVG dom element.
     */
    configureContainer() {
        this.content
            .attr("class", this.classContainer)
            .style("display", "flex")
            .style("flex-wrap", "wrap")
            .style("width", "100%");
    }

    /**
     * Generate DOM artboard in the HTML DOM.
     * @param {selection} selection - d3 selection
     * @returns A d3.js selection.
     */
    generateArtboard(selection) {
        return selection
            .selectAll("div")
            .data([{height: this.height, width: this.width}])
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate DOM element to hold content that can not be trimmed by artboard.
     * @param {selection} selection - d3 selection
     * @returns A d3.js selection.
     */
    generateContainer(selection) {
        return selection
            .selectAll(`.${this.classContainer}`)
            .data(d => [d])
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

}

export { DomGrid };
export default DomGrid;