import { ChartLabel } from "./index.js";

/**
 * QuadrantLabel is a label abstraction for a quadrant visualization.
 * @param {object} artboard - visualization svg
 */
class QuadrantLabel extends ChartLabel {

    constructor(artboard, Data) {

        // initialize inheritance
        super(artboard, Data);

    }

    /**
     * Determine quadrant label x.
     * @returns A string representing x axis label.
     */
    get xAxis() {
        return this.Data.x[2];
    }

    /**
     * Determine quadrant label y.
     * @returns A string representing x axis label.
     */
    get yAxis() {
        return this.Data.y[2];
    }

}

export { QuadrantLabel };
export default QuadrantLabel;
