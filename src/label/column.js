import { ChartLabel } from "./index.js";

/**
 * ColumnLabel is a label abstraction for a column visualization.
 * @param {object} artboard - visualization svg
 */
class ColumnLabel extends ChartLabel {

    constructor(artboard, Data, chartHeight, seriesScale, columnHeight, columnScale, columnY) {

        // initialize inheritance
        super(artboard, Data);

        // update self
        this.columnHeight = columnHeight;
        this.columnScale = columnScale;
        this.columnY = columnY;
        this.leaders = this.leaderedLabels(chartHeight);
        this.coordinates = this.yCoordinates(chartHeight);
        this.seriesScale = seriesScale;

    }

    /**
     * Prune labels to which need to be leadered.
     * @param {height} float - height of visual label represents
     * @returns An object where key/values .
     */
    leaderedLabels(height) {

        // get labels where a single line is too tall for visual it represents
        let labels = {};
        this.Data.data.forEach(d => labels[d.series] = d.values.filter(dd => this.singleIsTooTall(dd, this.columnHeight(dd))));

        // organize into top and bottom sets
        let result = {};
        Object.keys(labels).forEach(k => {
            result[k] = {
                top: labels[k].filter(d => this.columnY(d) < (height / 2)),
                bottom: labels[k].filter(d => this.columnY(d) >= (height / 2))
            }
        });

        return result;

    }

    /**
     * Determine column label x.
     * @param {d} object - d3.js stack object
     * @returns A float representing pixel location for x value.
     */
    x(d) {
        return this.seriesScale.bandwidth() / 2;
    }

    /**
     * Determine column label y.
     * @param {d} object - d3.js stack object
     * @returns A float representinfg pixel location for y value.
     */
    y(d, y) {
        return this.determineHeight(this.Data.extractValue(d));
    }

    /**
     * Determine column label y offset for leader lined labels.
     * @param {d} object - d3.js stack object
     * @returns A float representinfg pixel location for y value.
     */
    yOffset(d) {

        // get ids of items requiring offset
        let keys = Object.keys(this.coordinates[this.Data.extractSeries(d)]);

        return keys.includes(d.key) ? this.coordinates[this.Data.extractSeries(d)][d.key] : { y: this.columnY(d), yDiff: 0 };

    }

    /**
    * Determine column label y offset for leader lined labels.
    * @param {height} float - d3.js stack object
    * @returns A float representinfg pixel location for y value.
    */
    xCoordinates(height) {

        let result = {};

        // loop through series
        Object.keys(this.leaders).forEach(k => {

            let vals = {};

        });

        return result;

    }

    /**
     * Determine column label y offset for leader lined labels.
     * @param {height} float - d3.js stack object
     * @returns A float representinfg pixel location for y value.
     */
    yCoordinates(height) {

        let result = {};

        // loop through series
        Object.keys(this.leaders).forEach(k => {

            let vals = {};

            // loop through labels in the top of the visualization
            for (let i = this.leaders[k].top.length - 1; i >= 0; i--) {

                // set values
                let d = this.leaders[k].top[i];
                let initialY = this.columnY(d);
                let y = initialY;
                let textHeight = this.determineHeight(d) * 0.5;

                // skip top most label
                if (i < this.leaders[k].top.length -1) {

                    // set values
                    let previousD = this.leaders[k].top[i + 1];
                    let previous = this.columnY(previousD);
                    let lastY = vals[previousD.key].y + textHeight;

                    // if overlapping with previous
                    if (initialY < lastY) {

                        // adjust y
                        lastY += textHeight;
                        y = lastY;

                    }

                } else {

                    // adjust y
                    y = initialY + this.determineHeight(d);

                }

                // store data
                vals[d.key] = {
                    y: y,
                    usedOriginal: y == initialY,
                    yDiff: y - initialY
                };

            }

            // loop through labels in the top of the visualization
            for (let i = 0; i < this.leaders[k].bottom.length; i++) {

                // set values
                let d = this.leaders[k].bottom[i];
                let initialY = this.columnY(d);
                let y = initialY;
                let textHeight = this.determineHeight(d) * 0.5;

                // skip bottom most label
                if (i > 0) {

                    // set values
                    let nextD = this.leaders[k].bottom[i -1];
                    let next = this.columnY(nextD);
                    let lastY = vals[nextD.key].y - textHeight;

                    // if overlapping with next
                    if (initialY > lastY) {

                        // adjust y
                        lastY -= textHeight;
                        y = lastY;

                    }

                } else {

                    // adjust y
                    y = initialY - textHeight;

                }

                // store data
                vals[d.key] = {
                    y: y,
                    usedOriginal: y == initialY,
                    yDiff: y - initialY
                };

            };

            // update values
            result[k] = vals;

        });

        return result;

    }

}

export { ColumnLabel };
export default ColumnLabel;
