import { max, sum } from "d3-array";

import { simplifyLargeNumber } from "../utilities.js";

/**
 * ChartLabel is a label abstraction.
 * @param {object} artboard - visualization svg
 * @param {class} Data - JavaScript Class
 * @param {float} unit - artboard unit size
 */
class ChartLabel {

    constructor(artboard, Data, unit) {

        // update self
        this.artboard = artboard;
        this.Data = Data;
        this.unit = unit;
        this.units = this.size;

    }

    /**
     * Determine letter dimensions.
     * @returns An object where each key is a character and corresponding value its width in the svg artboard coordinate system.
     */
    get size() {

        let o = {};

        let characters = ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ".", ",", "-", "/", "_", "+", "=", " ", ":", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")"];

        // loop through characters
        characters.forEach(d => {

            // check for svg
            if(this.artboard.node().tagName.toLowerCase() == "svg") {

                // add to artboard
                let temp = this.artboard.append("text").text(d);

                // get dimensions
                let dimensions = typeof window === "undefined" ? { height: this.unit / 2, width: this.unit / 2 } : temp.node().getBBox();

                // remove node
                temp.remove();

                // update map
                o[d] = temp.text() == " " ? dimensions * 0.25 : dimensions;
            
            } else {

                // update map
                o[d] = { height: this.unit / 2, width: this.unit / 2 };

            }

        });

        return o;

    }

    /**
     * Determine dy of stacked label partials.
     * @param {float} bounds - max width for label
     * @param {integer} index - data index
     * @param {d} value - d3 datum object
     * @returns A string representing a valid dy value.
     */
    calculateDy(index, d, bounds) {
        return index == 0 ? ( this.ellipsize(simplifyLargeNumber(this.Data.extractValue(d)), bounds) == "" ? "0.3em" : "-0.1em") : `${index * 1.2}em`;
    }

    /**
     * Determine how to anchor text.
     * @param {float} degree - angle of text placement
     * @returns A string representing an svg text alignment enum (start, middle, end).
     */
    determineAlignment(degree) {
        let normalized = degree < 0 ? degree + 360 : degree;
        return normalized > 270 || (normalized > 0 && normalized < 90) ? "start" : (normalized > 100 && normalized < 240 ? "end" : "middle") ;
    }

    /**
     * Determine height of text in svg artboard coodinte system.
     * @param {string} text - text
     * @returns A float representing svg width in pixels.
     */
    determineHeight(text) {
        return max(text.toString().split("").map(d => this.units[d]), d => d.height);
    }

    /**
     * Determine width of text in svg artboard coodinte system.
     * @param {string} text - text
     * @returns A float representing svg width in pixels.
     */
    determineWidth(text) {
        return sum(text.toString().split("").map(d => this.units[d]), d => d.width);
    }

    /**
     * Format long labels with ellipsis when necessary.
     * @param {float} bounds - length text cannot exceed
     * @param {string} label - text label
     * @returns A d3.js selection.
     */
    ellipsize(label, bounds) {

        let l = label.toString();
        let a = l.split("");

        let letterCount = 0;
        let subText = "";
        let subWidth = 0;

        // determine how many letters will fit
        while (subWidth < bounds) {

            // check if there are more letters to add
            if (a[letterCount]) {

                // add characters
                subText += a[letterCount];
                subWidth = this.determineWidth(subText);

                // increment
                letterCount++;

            } else {

                subWidth = bounds;

            }

        }

        // if ellipsis is needed
        if (letterCount < l.length && bounds > 0) {

            subText = `${l.substring(0, letterCount)}...`;
            subWidth = this.determineWidth(subText);

            // determine how much of label we can keep while attaching the ellipsis
            while (subWidth > bounds) {

                // remove letter
                letterCount--;

                if (letterCount == 0) {

                    subWidth = bounds;

                } else {

                    // recalculate text size
                    subText = `${l.substring(0, letterCount)}...`;
                    subWidth = this.determineWidth(subText);

                }

            }

        }

        return subText == "..." ? "" : subText;

    }

    /**
     * Determine the text to display for label partials.
     * @param {float} bounds - max width for label
     * @param {integer} index - data index
     * @param {string} label - label partial value
     * @param {float} value - value partial value
     * @returns A string representing a label.
     */
    format(index, label, value, bounds) {

        let l = this.ellipsize(label, bounds);
        let labelIsTooLong = l == "";

        let v = this.ellipsize(value ? value.toLocaleString() : "", bounds);
        let valueIsTooLong = v == "";

        let valueSimple = this.ellipsize(simplifyLargeNumber(value), bounds);
        let valueSimpleIsTooLong = valueSimple == "";

        return index == 0 ? l : (labelIsTooLong ? "" :  (valueIsTooLong ? (valueSimpleIsTooLong ? "" : valueSimple) : (value.toString().length < 4 ? value : valueSimple)));

    }

    /**
     * Determine if flat label partials is taller than visual it represents.
     * @param {d} value - d3 datum object
     * @param {height} - height of visual label represents
     * @returns A boolean where true are too tall.
     */
    singleIsTooTall(d, height) {
        return this.determineHeight(this.Data.extractLabel(d)) > height;
    }

    /**
     * Determine if flat label partials is wider than visual it represents.
     * @param {d} value - d3 datum object
     * @param {width} - width of visual label represents
     * @returns A boolean where true are too tall.
     */
    singleIsTooWide(d, width) {
        return this.determineWidth(`${this.Data.extractLabel(d)} ${this.Data.extractValue(d)}`) > width;
    }

    /**
     * Determine if stacked label partials is taller than visual it represents.
     * @param {d} value - d3 datum object
     * @param {height} - height of visual label represents
     * @returns A boolean where true are too tall.
     */
    stackIsTooTall(d, height) {
        return this.determineHeight(this.Data.extractLabel(d)) * 2 > height;
    }

}

export { ChartLabel };
export default ChartLabel;
