import moment from "moment";

/**
 * Control how often a noisy event firing executes its callback.
 * @param {function} callback - event callback function to execute
 * @param {integer} wait - how long to wait before executing
 * @returns A callback function.
 */
function debounce(callback, wait) {
    let timeout;
    return (...args) => {
        const context = this;
        clearTimeout(timeout);
        timeout = setTimeout(() => callback.apply(context, args), wait);
    };
}

/**
 * Convert degree value to radian value.
 * @param {float} degree - angle of circle
 * @returns A float representing the specified angle in radians.
 */
function degreeToRadian(degree) {
    return degree * (Math.PI/180);
}

/**
 * Format with thousands comma separator.
 * @param {integer} value - value to format with a comma for thousands sets
 * @returns A string representing a number with comma separator for thousands.
 */
function formatThousands(value) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

/**
 * Determine largest divisor.
 * @param {float} maxValue - maximum value from value set
 * @returns An integer representing the largest integer divisor for value range.
 */
function largestDivisor(maxValue) {
    
    // get upper integer boundary of max value
    let maxInteger = Math.ceil(maxValue);

    return [...Array(maxInteger).keys()]
        .filter(d => d > 0)
        .sort((a,b) => b - a)
        .map(d => maxInteger % d == 0 ? d : null)
        .filter(d => d)[0];

}

/**
 * Convert calendar digit to leading zero string.
 * @param {integer} dateValue - calendar month of date value
 * @returns A leading zero digit represented as a string.
 */
function leadingZeroDate(dateValue) {
    return dateValue < 10 ? `0${dateValue}` : dateValue.toString();
}

/**
 * Determine the closest number to value which is divisible by factor.
 * @param {integer} value - number near
 * @param {integer} factor - dividable value
 * @returns An integer closest to value and divisible by factor.
 */
function nearestNumber(value, factor) {

    // get quotient
    let q = parseInt(value / factor);

    // first possible nearest number
    let n1 = factor * q;

    // sendon possible nearest number
    let n2 = (value * factor) > 0 ? (factor * (q + 1)) : (factor * (q - 1));

    return (Math.abs(value - n1) < Math.abs(value - n2)) ? n1 : n2;

}

/**
 * Convert a list of items to a narrative partial with oxford comma formatting.
 * @param {array} time - list of strings
 * @param {boolean} shouldCapitalize - TRUE if items should be capitalized
 * @returns A string where each item is delimited by  oxford comma.
 */
function oxfordComma(items, shouldCapitalize=false) {

    // check if we should capitalize
    // this will go away once source data is fixed
    if (shouldCapitalize) {

        // capitalize
        items = items.map(d => d.split(" ").map(w => `${w[0].toUpperCase()}${w.substr(1)}`).join(" "));

    }

    // get last item
    let lastItem = items[items.length - 1];
    let firstItems = items.slice(0, items.length - 1);
    let itemList;

    // update array to string
    if (firstItems.length > 1) {
        itemList = firstItems.join(", ") + ",";
    }
    else {
        itemList = firstItems.join(" ");
    }

    // replace for oxford commas
    return items.length > 1 ? itemList + ` and ${lastItem}` : items[0];

}

/**
 * Convert radian value to degree value.
 * @param {float} radian - angle of circle
 * @returns A float representing the specified angle in degrees.
 */
function radianToDegree(radian) {
    return radian * (180/Math.PI);
}

/**
 * Round value to 2 decimal places.
 * @param {float} value - data value
 * @returns A float rounded.
 */
function rounded(value) {
    return Math.round((value + Number.EPSILON) * 100) / 100;
}

/**
 * Round a value to nearest integer.
 * @param {float} value - data value
 * @returns An integer.
 */
function roundToNearestInteger(value) {

    let v;
    const digits = value ? value.toString().split(".")[0].length : 0;

    // determine how large the value
    switch(true) {
        case (digits === 3):
            v = Math.round(value / 100) * 100;
            break;
        case (digits > 3 && digits <= 7):
            v = Math.round(value / 1000) * 1000;
            break;
        case (digits > 7 && digits <= 10):
            v = Math.round(value / 1000000) * 1000000;
            break;
        case (digits > 10 && digits <= 13):
            v = Math.round(value / 1000000000) * 1000000000;
            break;
        default:
            v = Math.round(value);
    }

    return v;

}

/**
 * Format value for 2 characters, i.e. round super large integers.
 * @param {integer} value - value mapped to visualization element
 * @returns A string representing a formatted number value.
 */
function simplifyLargeNumber(value) {

    let v;

    // round value
    let rounded = roundToNearestInteger(value);

    // determine how large the value
    switch(true) {
        case (rounded >= 100 && rounded < 1000):
            v = `${rounded / 100}C`;
            break;
        case (rounded >= 1000 && rounded < 100000):
            v = rounded / 1000 < 1 ? `${(rounded / 100).toFixed(1)}C` : `${(rounded / 1000).toFixed(1)}K`;
            break;
        case (rounded >= 100000 && rounded < 100000000):
            v = rounded / 1000000 < 1 ? `${(rounded / 1000).toFixed(1)}K` : `${(rounded / 1000000).toFixed(1)}M`;
            break;
        case (rounded >= 100000000 && rounded < 100000000000):
            v = rounded / 1000000000 < 1 ? `${(rounded / 1000000).toFixed(1)}M` : `${(rounded / 1000000000).toFixed(1)}B`;
            break;
        default:
            v = rounded;
    }

    return v;

}

export { debounce, degreeToRadian, formatThousands, largestDivisor, leadingZeroDate, nearestNumber, oxfordComma, radianToDegree, rounded, roundToNearestInteger, simplifyLargeNumber }
