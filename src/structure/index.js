import { simplifyLargeNumber } from "../utilities.js";

/**
 * ChartData is a data abstraction.
 * @param {object} data - usually array; occasionally object
 */
class ChartData {

    constructor(data, params=null) {

        // update self
        this.source = data ? JSON.parse(JSON.stringify(data)) : data;
        this.params = params;
        this.data = this.condition(data, params);

    }

    /**
     * Build data object intersecting with layout parameters.
     * @returns A d3 pack data object.
     */
    get update() {
        return this.condition(this.source, this.params);
    }

    /**
     * Condition data for visualization requirements.
     * @param {object} data - usually array; occasionally object
     * @param {object} params - arbitrary key/value pairs
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    condition(data, params) {
        return this.enrichId(data);
    }

    /**
     * Generate event detail object.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @returns An object of key/value metadata pairs.
     */
    configureEventDetails(d,e) {
        return {
            id: this.extractId(d),
            label: this.extractLabel(d),
            value: this.extractValue(d),
            valueLabel: this.extractValueAsLabel(d),
            xy: [e.clientX, e.clientY]
        };
    }

    /**
     * Enrich data with id if missing.
     * @param {data} object - usually array; mapped object of visualization values
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    enrichId(data) {
        return data ? data.map((d,i) => {

            let result = d;
            if (!Object.keys(d).includes("id")) result.id = i;
            return d;

        }) : data;
    }

    /**
     * Get id from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum id.
     */
    extractId(d) {
        return d.id;
    }

    /**
     * Get label from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum label.
     */
    extractLabel(d) {
        return d.label;
    }

    /**
     * Get radius from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum radius value.
     */
    extractRadius(d) {
        return d.radius;
    }

    /**
     * Get value from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum value.
     */
    extractValue(d) {
        return d.value;
    }

    /**
     * Get value from data formatted as a label, i.e. mainly simplified for small spaces.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum value in more narrative form.
     */
    extractValueAsLabel(d) {
        return simplifyLargeNumber(this.extractValue(d));
    }

    /**
     * Get x from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum value.
     */
    extractX(d) {
        return d.x;
    }

    /**
     * Get y from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum value.
     */
    extractY(d) {
        return d.y;
    }

}

export { ChartData };
export default ChartData;
