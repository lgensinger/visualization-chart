import moment from "moment";
import { group } from "d3-array";

import { TimeData } from "./time.js";

/**
 * CalendarData is a data abstraction for calendar structures.
 * @param {array} data - object with key/value pairs of start/end.
 */
class CalendarData extends TimeData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

    }

    /**
     * Extract events from datum.
     * @param {d} object - d3.js data object
     * @returns An array of objects where each represents an event.
     */
    extractEvents(d) {
        return this.data.get(d);
    }

    /**
     * Get label from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum label.
     */
    extractLabel(d) {
        return d.event_title;
    }

    /**
     * Get value from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum value.
     */
    extractValue(d) {
        return `${d.date_start}:${d.date_end}`;
    }

    /**
     * Get value from data formatted as a label, i.e. mainly simplified for small spaces.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum value in more narrative form.
     */
    extractValueAsLabel(d) {
        return `${d.durationDate} day${d.durationDate == 1 ? "" : "s"}`;
    }

}

export { CalendarData };
export default CalendarData;
