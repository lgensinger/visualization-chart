import { ScatterData } from "./scatter.js";

/**
 * ClusterData is a data abstraction for clustered x/y structures.
 * @param {array} data - array of objects with key/value pairs of id, label, description, x, y.
 */
class ClusterData extends ScatterData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

    }

    /**
     * Condition data for visualization requirements.
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    condition(data, params) {
        return data ? data.map((d,i) => {

            let result = d;
            if (!Object.keys(d).includes("id")) result.id = i;

            // because cluster algorithms often set the x/y during layout
            // set those values on placeholder keys
            d.xx = d.x;
            d.yy = d.y;

            return result;

        }) : data;
        
    }

    /**
     * Get radius from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum radius value.
     */
    extractRadius(d) {
        return d.value;
    }

    /**
     * Get x from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum value.
     */
    extractX(d) {
        return d.xx;
    }

    /**
     * Get y from data.
     * @param {d} object - d3.js data object
     * @returns A float representing a datum value.
     */
    extractY(d) {
        return d.yy;
    }

}

export { ClusterData };
export default ClusterData;
