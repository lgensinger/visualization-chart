import { ChartData } from "./index.js";

/**
 * ScatterData is a data abstraction for x/y structures.
 * @param {array} data - array of objects with key/value pairs of id, label, description, x, y.
 */
class ScatterData extends ChartData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

    }

    /**
     * Generate event detail object.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @returns An object of key/value metadata pairs.
     */
    configureEventDetails(d,e) {
        return {
            id: this.extractId(d),
            label: this.extractLabel(d),
            value: this.extractValue(d),
            valueLabel: this.extractValueAsLabel(d),
            x: this.extractX(d),
            y: this.extractY(d),
            xy: [e.clientX, e.clientY]
        };
    }

}

export { ScatterData };
export default ScatterData;
