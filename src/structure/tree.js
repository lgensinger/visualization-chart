import { sum } from "d3-array";
import { hierarchy, stratify } from "d3-hierarchy";

import { configurationData } from "../configuration.js";
import { ChartData } from "./index.js";

/**
 * TreeData is a data abstraction for tree structures.
 * @param {array} data - object with key/value pairs of id, label, value
 * @param {object} params - arbitrary key/value pairs
 */
class TreeData extends ChartData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.delimiter = params.delimiter;
        this.data = this.condition(data, params);

    }

    /**
     * Condition data for visualization requirements.
     * @param {array} data - object with key/value pairs of id, label, value
     * @param {object} params - arbitrary key/value pairs
     * @returns An array of self-referencing nodes.
     */
    condition(data, params) {

        let result = null;

        // use path as id if missing 
        data.forEach(d => { if (!Object.keys(d).includes("id")) d.id = d.path });

        // build hierarchy from flat data
        let hierarchy = stratify()
            .id(d => d.path)
            .parentId(d => d.path.substring(0, d.path.lastIndexOf(this.delimiter)));

        // check for required params
        if (this.delimiter) {

            try {

                // see if data is already structured
                hierarchy(data);

            } catch (error) {

                let nodes = [];

                // build data into single root
                data.forEach(d => {

                    let dd = d;

                    // add path
                    dd.path = `root${this.delimiter}${d.id}`;

                    // add to node list
                    nodes.push(dd);

                });

                // add root
                nodes.push({
                    id: "root",
                    path: "root",
                    value: sum(data, d => d.value)
                });

                // update data
                data = nodes;

            }

            // build nest
            let nestedData = hierarchy(data)
                .sort((a,b) => a.id.toLowerCase().localeCompare(b.id.toLowerCase()));

            // calculate layout leaves
            // can't get suming working properly
            result = nestedData
                .sum((d) => 1)
                .sort((a,b) => b.value - a.value);

        }

        return result;

    }

    /**
     * Get id from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum id.
     */
    extractId(d) {
        return d.data.path;
    }

    /**
     * Get label from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a datum label.
     */
    extractLabel(d) {
        return d.data.path.split(this.delimiter)[d.data.path.split(this.delimiter).length - 1];
    }

}

export { TreeData };
export default TreeData;
