import { hierarchy } from "d3-hierarchy";

import { ChartData } from "./index.js";

/**
 * StackData is a data abstraction for stacked series structures.
 * @param {array} data - object with key/value pairs of seriesName: id, label, value.
 * @param {object} params - key/value pairs needed for getters
 */
class SeriesData extends ChartData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

    }

    /**
     * Condition data for visualization requirements.
     * @param {array} data - object with key/value pairs of id, label, value
     * @param {object} params - key/value pairs needed for getters
     * @returns An object where keys are series' set label and corresponding values are a object where keys are series' labels and values are each series value.
     */
    condition(data, params) {

        let result = null;

        // check for required params
        if (data) {

            result = {};

            Object.keys(data).forEach(k => {
                result[k] = {};
                Object.keys(data[k]).forEach((kk,i) => {
                    result[k][kk] = {
                        id: `${k}-${i}`,
                        label: kk,
                        series: k,
                        value: data[k][kk]
                    }
                })
            });

        }

        return result;

    }

    /**
     * Generate event detail object.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @returns An object of key/value metadata pairs.
     */
    configureEventDetails(d, e) {
        return {
            id: this.extractId(d),
            label: this.extractLabel(d),
            series: this.extractSeries(d),
            value: this.extractValue(d),
            valueLabel: this.extractValueAsLabel(d),
            xy: [e.clientX, e.clientY]
        };
    }

    /**
     * Get series name from data.
     * @param {d} object - d3.js data object
     * @returns A string representing a series name.
     */
    extractSeries(d) {
        return d.series;
    }

}

export { SeriesData };
export default SeriesData;
