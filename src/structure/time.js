import moment from "moment";
import { group } from "d3-array";

import { ChartData } from "./index.js";

/**
 * TimeData is a data abstraction for temporal structures.
 * @param {array} data - array of objects with key/value pairs of id, date, label, value.
 * @param {object} params - key/value pairs needed for getters
 */
class TimeData extends ChartData {

    constructor(data, params) {

        // initialize inheritance
        super(data, params);

        // update self
        this.timezone = params.timezone || "en-us";

    }

    /**
     * Generate iso 8601 strings of the first day of months.
     * @returns An array of strings where each represents a month in the bounds of start to end.
     */
    get months() {
        return moment.months();
    }

    /**
     * Generate weekdays starting based on locale.
     * @returns An array of weekday names.
     */
    get weekdays() {
        moment.updateLocale(this.timezone, {week:{dow:1}});
        return moment.localeData(this.timezone).weekdays(true);
    }

    /**
     * Condition data for visualization requirements.
     * @returns An object or array of data specifically formatted to the visualization type.
     */
    condition(data, params) {

        let result = data;

        // determine how to enrich
        switch(params.timeFill) {
            case "date": result = this.fillDates(data); break;
            case "hour": result = this.fillHours(data); break;
        }

        return result;
        
    }

    /**
     * Generate event detail object.
     * @param {d} object - d3.js data object
     * @param {e} Event - JavaScript Event object
     * @returns An object of key/value metadata pairs.
     */
    configureEventDetails(d,e) {
        let events = this.extractEvents(d);
        return {
            events: events ? events : [],
            id: this.extractId(d),
            label: this.extractLabel(d),
            value: this.extractValue(d),
            valueLabel: this.extractValueAsLabel(d),
            xy: [e.clientX, e.clientY]
        };
    }

    /**
     * Extract events from datum.
     * @param {d} object - d3.js data object
     * @returns An array of objects where each represents an event.
     */
    extractEvents(d) {
        return d.events;
    }

    /**
     * Create datum for dates inbetween start/end.
     * @returns A map where keys are 8601 date values and corresponding values are arrays of objects where each represents an event span for the date.
     */
    fillDates(data) {

        // create new data objects in between start/end
        // with additional values to represent the span of time the original duration covers
        let nd = data.map(d => this.generateDates(d.time_start,d.time_end)
            .map(x => ({...{
                date: x, 
                day: moment(x).weekday(),
                durationDate: moment(d.time_end).diff((moment(d.time_start)),"day") + 1,
                durationHour: moment(d.time_end).diff((moment(d.time_start)),"hour"),
                renderDate: d.time_start === x ? x : moment(x).startOf("isoweek").format("YYYY-MM-DD"),
                renderLength: moment(d.time_end).diff(moment(x).endOf("isoweek"),"day") < 0 ? moment(d.time_end).diff((moment(x)),"day") : moment(x).endOf("isoweek").diff(moment(x),"day"),
                renderOffset: data.filter(y => {
                    let weekStart = moment(x).startOf("isoweek").format("YYYY-MM-DD");
                    let currentIsWeekStartAndNotEventStart = x === weekStart && x !== y.time_start;
                    let currentIsAfterOrSameAsEventStart = moment(x).diff(moment(y.time_start),"day") > 0;
                    let currentIsBeforeOrSameAsEventEnd = moment(x).diff(moment(y.time_end),"day") <= 0;
                    return currentIsAfterOrSameAsEventStart && currentIsBeforeOrSameAsEventEnd && !currentIsWeekStartAndNotEventStart;
                }).length
            },...d})))
            .flat()
            .map((d,i) => ({...{ id: i},...d}));

        // generate map so that a given date corresponds to discrete values
        // representing the original data as it occurs in the span of each date
        return group(nd, d => d.date);
    }

    /**
     * Create datum for hours inbetween start/end.
     * @returns A map where keys are 8601 time values and corresponding values are arrays of objects where each represents an event span for the date.
     */
    fillHours(data) {
        
        // create new data objects in between start/end
        // with additional values to represent the span of time the original duration covers
        let nd = data.map(d => this.generateHours(d.time_start,d.time_end)
            .map(x => ({...{
                date: x.split(" ")[0], 
                day: moment(`${x.split(" ")[0]}T${x.split(" ")[1]}:00:00.000Z`).weekday(),
                durationMinute: moment(d.time_end).diff((moment(d.time_start)),"minute"),
                hour: x.split(" ")[1],
                timestamp: moment(`${x.split(" ")[0]}T${x.split(" ")[1]}:00:00.000Z`).utc().format()
            },...d})))
            .flat()
            .map((d,i) => ({...{ id: i},...d}));

        // generate map so that a given date corresponds to discrete values
        // representing the original data as it occurs in the span of each date
        return group(nd, d => d.hour);

    }

    /**
     * Generate iso 8601 strings of the days between start and end.
     * @param {string} end - iso 8601 date value
     * @param {string} start - iso 8601 date value
     * @returns An array of strings where each represents a date in the bounds of start to end.
     */
    generateDates(start, end) {

        let result = [];

        let dateEnd = moment(end);
        let dateStart = moment(start);

        // loop through dates within boundary
        while (dateStart < dateEnd || !result.includes(dateEnd.format("YYYY-MM-DD"))) {

            // capture actual date iso string since moment mutates values
            let date = dateStart.format("YYYY-MM-DD");

            // update list
            result.push(date);

            // iterate the date value
            dateStart.add(1, "day");

        }

        return result;

    }

    /**
     * Generate iso 8601 strings of the days between start and end.
     * @param {string} end - iso 8601 date value
     * @param {string} start - iso 8601 date value
     * @returns An array of strings where each represents a date in the bounds of start to end.
     */
    generateHours(start, end) {

        let result = [];

        let hourEnd = moment(end).utc();
        let hourStart = moment(start).utc();

        // loop through hours between start/end dates
        while (hourStart < hourEnd || !result.includes(hourEnd.format("YYYY-MM-DD HH"))) {

            // capture value string since moment mutates values
            let dateHour = hourStart.format("YYYY-MM-DD HH");

            // update list
            result.push(dateHour);

            // iterate the date value
            hourStart.add(1, "hour");

        }

        return result;

    }

}

export { TimeData };
export default TimeData;
