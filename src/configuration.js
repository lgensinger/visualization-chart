import packagejson from "../package.json";

const configuration = {
    branding: process.env.LGV_LABEL || "lgv",
    name: packagejson.name.split("/")[1]
};

const configurationData = {
    delimiter: process.env.LGV_PARSE_DELIMITER || "."
}

const configurationLayout = {
    height: process.env.LGV_HEIGHT || 500,
    width: process.env.LGV_WIDTH || 500
}

const configurationPack = {
    padding: process.env.LGV_PACK_PADDING || 1
};

const configurationRing = {
    innerRadius: process.env.LGV_RING_INNER_RADIUS || 0.7
}

const configurationSeries = {
    labelsOnTop: process.env.LGV_SERIES_LABELS_ON_TOP || true,
    normalize: process.env.LGV_SERIES_NORMAILIZE || false,
    padding: process.env.LGV_SERIES_PADDING || 0.25,
    useLabels: process.env.LGV_SERIES_LABELS || true
};

export { configuration, configurationData, configurationLayout, configurationPack, configurationRing, configurationSeries };
export default configuration;
