import { VisualizationLegend } from "./index.js";
import { configuration } from "../configuration.js";

/**
 * ActivityLegend is an activity chart legend abstraction.
 * @param {array} items - key/value pairs of label,swatch,value
 * @param {string} label - identifer for chart brand
 * @param {string} name - name of chart
 */
class ActivityLegend extends VisualizationLegend {

    constructor(data, label=configuration.branding, name=configuration.name) {

        // initialize inheritance
        super(data, label, name);

        // update self
        this.classLabelContainer = `${label}-${name}-legend-label-set`;
        this.classSwatchContainer = `${label}-${name}-legend-swatch-set`;
        this.labelContainer = null;
        this.swatchContainer = null;

    }

    /**
     * Position and minimally style label DOM element.
     */
    configureLabel() {
        this.label
            .attr("class", this.classLabel)
            .style("border-left", (d,i) => i == 0 ? "none" : "0.15em solid")
            .style("box-sizing", "border-box")
            .style("color", (d,i) => i == 0 ? "transparent": "currentColor")
            .style("padding-left","0.25em")
            .style("padding-top","1em")
            .style("width","100%")
            .html(d => d);
    }

    /**
     * Position and minimally style label group DOM element.
     */
    configureLabelContainer() {
        this.labelContainer
            .attr("class", this.classLabelContainer)
            .style("display","flex")
            .style("left",0)
            .style("position","absolute")
            .style("top",0)
            .style("width","100%")
    }

    /**
     * Position and minimally style swatch group DOM element.
     */
    configureSwatchContainer() {
        this.swatchContainer
            .attr("class", this.classSwatchContainer)
            .style("display","flex");
    }

    /**
     * Position and minimally style swatch DOM element.
     */
    configureSwatch() {
        this.swatch 
            .attr("class", this.classSwatch)
            .style("background-color", d => d)
            .style("border", (d,i) => i == 0 ? "0.05em solid" : "none")
            .style("box-sizing", "border-box")
            .style("height", "1em")
            .style("width", "100%");
    }

    /**
     * Generate legend label in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateLabel(selection) {
        return selection
            .selectAll(`.${this.classLabel}`)
            .data(d => d)
            .join(
                enter => enter.append("span"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate label group in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateLabelContainer(domNode) {
        return domNode
            .selectAll(`.${this.classLabelContainer}`)
            .data([this.data.map(d => d.label)])
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate swatch group in DOM element.
     * @param {selection} domNode - d3 selection
     * @returns A d3.js selection.
     */
    generateSwatchContainer(domNode) {
        return domNode
            .selectAll(`.${this.classSwatchContainer}`)
            .data([this.data.map(d => d.swatch)])
            .join(
                enter => enter.append("div"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate legend.
     */
    generateLegend() {

        // update style on dom node container
        this.legend.style("position","relative");

        // generate core elements
        this.swatchContainer = this.generateSwatchContainer(this.legend);
        this.configureSwatchContainer();

        this.labelContainer = this.generateLabelContainer(this.legend);
        this.configureLabelContainer();

        this.swatch = this.generateSwatch(this.swatchContainer);
        this.configureSwatch();

        this.label = this.generateLabel(this.labelContainer);
        this.configureLabel();

    }

    /**
     * Generate legend swatch in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateSwatch(selection) {
        return selection
            .selectAll(`.${this.classSwatch}`)
            .data(d => d)
            .join(
                enter => enter.append("span"),
                update => update,
                exit => exit.remove()
            );
    }

}

export { ActivityLegend };
export default ActivityLegend;