import { select } from "d3-selection";

import { configuration } from "../configuration.js";

/**
 * VisualizationLegend is a chart legend abstraction.
 * @param {array} data - key/value pairs of label,swatch,value
 * @param {string} label - identifer for chart brand
 * @param {string} name - name of chart
 */
class VisualizationLegend {

    constructor(data, label=configuration.branding, name=configuration.name) {

        // update self
        this.classItem = `${label}-${name}-legend-item`;
        this.classLabel = `${label}-${name}-legend-label`;
        this.classSwatch = `${label}-${name}-legend-swatch`;
        this.data = data;
        this.item = null;
        this.label = null;
        this.legend = null;
        this.swatch = null;

    }

    /**
     * Position and minimally style legend in dom element.
     */
    configureItem() {
        this.item
            .attr("class", this.classItem)
            .style("align-items","center")
            .style("display", "flex")
            .style("margin-left","0.5em");
    }

    /**
     * Position and minimally style legend label in dom element.
     */
    configureLabel() {
        this.label
            .attr("class", this.classLabel)
            .html(d => d.label);
    }

    /**
     * Position and minimally style legend swatch in dom element.
     */
    configureSwatch() {
        this.swatch 
            .attr("class", this.classSwatch)
            .style("background-color", d => d.swatch)
            .style("display", "block")
            .style("margin-right", "0.25em")
            .style("height", "1em")
            .style("width", "1em");
    }

    /**
     * Generate legend item in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateItem(selection) {
        return selection
            .selectAll(`.${this.classItem}`)
            .data(this.data)
            .join(
                enter => enter.append("p"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate legend label in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateLabel(selection) {
        return selection
            .selectAll(`.${this.classLabel}`)
            .data(d => [d])
            .join(
                enter => enter.append("span"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Generate legend.
     */
    generateLegend() {

        this.item = this.generateItem(this.legend);
        this.configureItem();

        this.swatch = this.generateSwatch(this.item);
        this.configureSwatch();

        this.label = this.generateLabel(this.item);
        this.configureLabel();

    }

    /**
     * Generate legend color swatch in DOM element.
     * @param {selection} selection - d3.js selection
     */
    generateSwatch(selection) {
        return selection
            .selectAll(`.${this.classSwatch}`)
            .data(d => [d])
            .join(
                enter => enter.append("span"),
                update => update,
                exit => exit.remove()
            );
    }

    /**
     * Render visualization in dom.
     * @param {node} domNode - HTML node
    */
    render(domNode) {

        // update self
        this.legend = select(domNode);

        // render in dom
        this.generateLegend();

    }

    /**
     * Update legend.
     * @param {object} data - key/value pairs of label,swatch,value
     */
    update(data) {

        // update self
        this.data = data;

        // update render in dom
        this.generateLegend();

    }

}

export { VisualizationLegend };
export default VisualizationLegend;