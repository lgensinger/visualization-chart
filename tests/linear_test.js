import test from "ava";

import { configuration, configurationLayout } from "../src/configuration.js";
import { LinearGrid } from "../src/index.js";

let testRadius = 300;

/******************** EMPTY VARIABLES ********************/

// initialize
let vc = new LinearGrid();

// TEST INIT //
test("init", t => {

    t.true(vc !== undefined);
    t.true(vc.heightSpecified == configurationLayout.height);
    t.true(vc.widthSpecified == configurationLayout.width);

});

// TEST RENDER //
test("render", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    vc.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");

});

/******************** DECLARED PARAMS ********************/

let testHeight = Math.floor(Math.random() * (100 - 1) + 1);
let testWidth = Math.floor(Math.random() * (100 - 1) + 1);

// initialize
let vcp = new LinearGrid([], testWidth, testHeight);

// TEST INIT //
test("initParams", t => {

    t.true(vcp !== undefined);
    t.true(vcp.heightSpecified == testHeight);
    t.true(vcp.widthSpecified == testWidth);

});

// TEST RENDER //
test("renderParams", t => {

    // clear document
    document.body.innerHTML = "";

    // render to dom
    vcp.render(document.body);

    // get generated element
    let artboard = document.querySelector(`.${configuration.name}`);

    t.true(artboard !== undefined);
    t.true(artboard.nodeName == "svg");

});
